package br.com.dell.coderswag.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.dell.coderswag.R
import br.com.dell.coderswag.model.Product
import kotlinx.android.synthetic.main.product_list_item.view.*

class ProductsAdapter(
    private val context: Context, private val products: List<Product>,
    private val layoutInflater: LayoutInflater, private val itemClick: (Product) -> (Unit)
) :
    RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder =
        ProductsViewHolder(layoutInflater.inflate(R.layout.product_list_item, parent, false))

    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) = holder.bind(position)

    inner class ProductsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val product = products[position]
            val identifier = context.resources.getIdentifier(
                product.image,
                "drawable",
                context.packageName
            )

            itemView.apply {
                this.ivProductImage.setImageResource(identifier)
                this.tvProductName.text = product.title
                this.tvProductPrice.text = product.price
                this.setOnClickListener { itemClick(product) }
            }
        }

    }

}