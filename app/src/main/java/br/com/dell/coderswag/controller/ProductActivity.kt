package br.com.dell.coderswag.controller

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import br.com.dell.coderswag.R
import br.com.dell.coderswag.adapters.ProductsAdapter
import br.com.dell.coderswag.services.DataService
import br.com.dell.coderswag.utilities.EXTRA_CATEGORY
import br.com.dell.coderswag.utilities.EXTRA_PRODUCT
import br.com.dell.coderswag.utilities.EXTRA_PRODUCT_BUNDLE
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        val categoryString = intent.getStringExtra(EXTRA_CATEGORY)

        productList.apply {
            this.adapter = ProductsAdapter(this@ProductActivity, DataService.getProducts(categoryString), layoutInflater) { product ->
                Intent(this@ProductActivity, ProductDetailActivity::class.java).let {
                    it.putExtra(EXTRA_PRODUCT, Bundle().apply { this.putParcelable(EXTRA_PRODUCT_BUNDLE , product) })
                    startActivity(it)
                }
            }

            this.layoutManager = GridLayoutManager(this@ProductActivity, getSpanCount())
        }
    }

    private fun getSpanCount(): Int {
        val configuration = resources.configuration
        return when {
            configuration.orientation == Configuration.ORIENTATION_LANDSCAPE -> 3
            configuration.orientation == Configuration.ORIENTATION_PORTRAIT -> 2
            configuration.screenWidthDp > 720 -> 3
            else -> 2
        }
    }

}
