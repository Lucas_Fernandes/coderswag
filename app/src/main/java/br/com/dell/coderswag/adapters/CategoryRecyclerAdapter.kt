package br.com.dell.coderswag.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.dell.coderswag.R
import br.com.dell.coderswag.model.Category
import kotlinx.android.synthetic.main.category_list_item.view.*

class CategoryRecyclerAdapter(
    private val context: Context,
    private val categories: List<Category>,
    private val layoutInflater: LayoutInflater,
    private val itemClick: (Category) -> (Unit)
) : RecyclerView.Adapter<CategoryRecyclerAdapter.CategoryRecyclerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryRecyclerHolder =
        CategoryRecyclerHolder(layoutInflater.inflate(R.layout.category_list_item, parent, false))

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: CategoryRecyclerHolder, position: Int) =
        holder.bind(position)

    inner class CategoryRecyclerHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {

            val category = categories[position]

            val imgIdentifier = context.resources.getIdentifier(
                category.image,
                "drawable",
                context.packageName
            )

            itemView.apply {
                this.categoryImage.setImageResource(imgIdentifier)
                this.categoryName.text = categories[position].title
                this.setOnClickListener { itemClick(category) }
            }
        }
    }

}

