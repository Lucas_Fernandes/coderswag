package br.com.dell.coderswag.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(val title: String, val price: String, val image: String) : Parcelable