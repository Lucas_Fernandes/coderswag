package br.com.dell.coderswag.controller

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.dell.coderswag.R
import br.com.dell.coderswag.model.Product
import br.com.dell.coderswag.utilities.EXTRA_PRODUCT
import br.com.dell.coderswag.utilities.EXTRA_PRODUCT_BUNDLE
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppCompatActivity() {

    private var product: Product? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        getIntentArguments()
        bindViews()
    }

    private fun getIntentArguments() {
        if (intent.hasExtra(EXTRA_PRODUCT)) {
            this.product = intent.getBundleExtra(EXTRA_PRODUCT).getParcelable(EXTRA_PRODUCT_BUNDLE)
        }
    }

    private fun bindViews() {
        ivProductDetailImage.setImageResource(resources.getIdentifier(this.product!!.image,
                                                                "drawable", packageName))
        tvProductDetailName.text = this.product!!.title
        tvProductDetailPrice.text = this.product!!.price
    }
}