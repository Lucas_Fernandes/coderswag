package br.com.dell.coderswag.controller

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.dell.coderswag.R
import br.com.dell.coderswag.adapters.CategoryRecyclerAdapter
import br.com.dell.coderswag.services.DataService
import br.com.dell.coderswag.utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        categoryList.apply {
            this.adapter = CategoryRecyclerAdapter(this@MainActivity,
                DataService.categories, layoutInflater) { category -> Intent(this@MainActivity, ProductActivity::class.java).let {
                    it.putExtra(EXTRA_CATEGORY, category.title)
                    startActivity(it)
                }
            }
            this.layoutManager = LinearLayoutManager(this@MainActivity)
            this.setHasFixedSize(true)
        }
    }
}
