package br.com.dell.coderswag.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Category(val title: String, val image: String) : Parcelable {
    override fun toString(): String {
        return title
    }
}