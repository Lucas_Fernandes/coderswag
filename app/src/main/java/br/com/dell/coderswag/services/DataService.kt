package br.com.dell.coderswag.services

import br.com.dell.coderswag.model.Category
import br.com.dell.coderswag.model.Product

object DataService {

    val categories = listOf(
        Category("SHIRTS", "shirtimage"),
        Category("HOODIES", "hoodieimage"),
        Category("HATS", "hatimage"),
        Category("DIGITAL", "digitalgoodsimage")
    )

    private val hats = listOf(
        Product("DevsLopes Graphic Beanie", "R$18,00", "hat1"),
        Product("DevsLopes Hat Black", "R$20,00", "hat2"),
        Product("DevsLopes Hat White", "R$18,00", "hat3"),
        Product("DevsLopes Hat Snapnack", "R$22,00", "hat4"),
        Product("DevsLopes Graphic Beanie", "R$18,00", "hat1"),
        Product("DevsLopes Hat Black", "R$20,00", "hat2"),
        Product("DevsLopes Hat White", "R$18,00", "hat3"),
        Product("DevsLopes Hat Snapnack", "R$22,00", "hat4"),
        Product("DevsLopes Graphic Beanie", "R$18,00", "hat1"),
        Product("DevsLopes Hat Black", "R$20,00", "hat2"),
        Product("DevsLopes Hat White", "R$18,00", "hat3"),
        Product("DevsLopes Hat Snapnack", "R$22,00", "hat4")
    )

    private val hoodies = listOf(
        Product("Devslopes Hoodie Gray", "R$28,00", "hoodie1"),
        Product("Devslopes Hoodie Red", "R$32,00", "hoodie2"),
        Product("Devslopes Gray Hoodie", "R$32,00", "hoodie3"),
        Product("Devslopes Black Hoodie", "R$32,00", "hoodie4"),
        Product("Devslopes Hoodie Gray", "R$28,00", "hoodie1"),
        Product("Devslopes Hoodie Red", "R$32,00", "hoodie2"),
        Product("Devslopes Gray Hoodie", "R$32,00", "hoodie3"),
        Product("Devslopes Black Hoodie", "R$32,00", "hoodie4"),
        Product("Devslopes Hoodie Gray", "R$28,00", "hoodie1"),
        Product("Devslopes Hoodie Red", "R$32,00", "hoodie2"),
        Product("Devslopes Gray Hoodie", "R$32,00", "hoodie3"),
        Product("Devslopes Black Hoodie", "R$32,00", "hoodie4")
    )


    private val shirts = listOf(
        Product("Devslopes Shirt Black", "R$18,00", "shirt1"),
        Product("Devslopes Badge Light Gray", "R$20,00", "shirt2"),
        Product("Devslopes Logo Shirt Red", "R$22,00", "shirt3"),
        Product("Devslopes Hussle", "R$22,00", "shirt4"),
        Product("Kickflip Studios", "R$18,00", "shirt5"),
        Product("Devslopes Shirt Black", "R$18,00", "shirt1"),
        Product("Devslopes Badge Light Gray", "R$20,00", "shirt2"),
        Product("Devslopes Logo Shirt Red", "R$22,00", "shirt3"),
        Product("Devslopes Hussle", "R$22,00", "shirt4"),
        Product("Kickflip Studios", "R$18,00", "shirt5"),
        Product("Devslopes Shirt Black", "R$18,00", "shirt1"),
        Product("Devslopes Badge Light Gray", "R$20,00", "shirt2"),
        Product("Devslopes Logo Shirt Red", "R$22,00", "shirt3"),
        Product("Devslopes Hussle", "R$22,00", "shirt4"),
        Product("Kickflip Studios", "R$18,00", "shirt5")
    )


    private val digitalGood = emptyList<Product>()

    fun getProducts(category: String): List<Product> {
        return when (category) {
            "SHIRTS" -> shirts
            "HATS" -> hats
            "HOODIES" -> hoodies
            else -> digitalGood
        }
    }

}