package br.com.dell.coderswag.utilities

const val EXTRA_CATEGORY = "category"
const val EXTRA_PRODUCT = "product"
const val EXTRA_PRODUCT_BUNDLE = "productBundle"